#ifndef __GRAPH__
#define __GRAPH__

#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring>
#include <vector>
#include <list>
#include <map>
#include <queue>
#include <stack>
#include <cstdint>
#include <set>
#include <climits>

using namespace std;

typedef string Type;


class Graph {
  public:
  private:
    struct Edge{
      Type index; //connecting node
      int weight; //distance frrom the current node to the connecting node
      Edge(Type i, int d = 1) : index(i), weight(d) {};
    };

    map<Type, list<Edge>> adjacencyList; //map of graph
    map<Type, bool> visited;
    map<Type, int> distance;
    void clearVisited(void);
    void clearDist(void);
    void depthFirstSearchRecurse(Type& node);
    void breathFirstSearchRecurse(Type& node);
    void depthFirstSearchIterative(Type& node);
    void breathFirstSearchIterative(Type& node);

  public:
//constructor && destructor
    Graph();
    ~Graph();
//////////////////////////////////////////////////////

    void addEdge(Type& node, Type& e);
    void addEdge(Type& node, Type& e, int weight);
    void remove(Type& node); // remove some node on graph
    void printGraph(void); // print graph
    void depthFirstSearch(Type& node);
    void breathFirstSearch(Type& node);
    void shortestPath(Type& startNode); // pass in start vertex and end vertex, returns how many nodes it passes through to get to end Node
};

#endif
