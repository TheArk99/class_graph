#include "graph.h"


Graph::Graph(){
}


Graph::~Graph(){
}


void Graph::addEdge(Type& node, Type& e){
  Edge edges(e);
  if (edges.index == node){
    return;
  }
  adjacencyList[node].push_front(edges);
}

void Graph::addEdge(Type& node, Type& e, int weight){
  Edge edges(e, weight);
  if (edges.index == node){
    return;
  }
  adjacencyList[node].push_front(edges);
}

//void remove(Type& node)

void Graph::printGraph(void){
    for (const auto& pair : adjacencyList) {
        cout << pair.first << ": ";
        for (const auto& item : pair.second) {
            cout << item.index << " ";
        }
        cout << endl;
    }
}



void Graph::clearVisited(void){
  /*
  auto itr = visited.begin();
  while (itr != visited.end()){
    if (itr->second == true){
      itr = visited.erase(itr);
    }else{
      itr++;
    }
  }
  */
  visited.clear();
}

void Graph::clearDist(void){
  /*
  auto itr = visited.begin();
  while (itr != visited.end()){
    if (itr->second == true){
      itr = visited.erase(itr);
    }else{
      itr++;
    }
  }
  */
  distance.clear();
}


void Graph::depthFirstSearch(Type& node){
  if (adjacencyList.find(node) == adjacencyList.end()){
    return;
  }
  clearVisited();
  cout << "starting node in DFS, recursive: " << node << endl;
  depthFirstSearchRecurse(node);
  cout << endl << endl;

  clearVisited();
  cout << "starting node in DFS, iterative: " << node << endl;
  depthFirstSearchIterative(node);

  cout << endl;
}


//recursive
void Graph::depthFirstSearchRecurse(Type& node){
  if (visited[node]){
    return;
  }
  visited[node] = true;
  cout << node << " ";
  list<Edge>::iterator it = (adjacencyList[node]).begin();
  while (it != (adjacencyList[node]).end()){
    if (!visited[it->index]){
      depthFirstSearchRecurse(it->index);
    }
    it++;
  }

}



//iterrative
void Graph::depthFirstSearchIterative(Type& node){
  stack<Type> stack; // Use a stack for DFS
  stack.push(node);
  while (!stack.empty()) {
    // Pop a vertex from stack and print it
    Type currentNode = stack.top();
    stack.pop();
    if (!visited[currentNode]) { // do only if it has not been visited
      cout << currentNode << " ";
      visited[currentNode] = true;
    }

    // For all adjacent vertices of currentNode
    // if it has not been visited, then push it onto the stack.
    for (auto i = adjacencyList[currentNode].begin(); i != adjacencyList[currentNode].end(); ++i) {
      if (!visited[i->index]) {
        stack.push(i->index);
      }
    }
  }
}


void Graph::breathFirstSearch(Type& node){
  if (adjacencyList.find(node) == adjacencyList.end()){
    return;
  }
  clearVisited();
  cout << "starting node in BFS, recursive: " << node << endl;
  cout << node << " ";
  breathFirstSearchRecurse(node);
  cout << endl;
  cout << endl;

  clearVisited();
  cout << "starting node in BFS, iterative: " << node << endl;
  breathFirstSearchIterative(node);

  cout << endl;
}

//dr hwang's version that is not recursive as i could not yet figure it out recursivly
//iterative
void Graph::breathFirstSearchIterative(Type& node){
  queue<Type> q;
  if (visited[node]){
    return;
  }

  q.push(node);
  visited[node] = true;

  while(!q.empty()){
    Type& currentNode = q.front();
    q.pop();
    cout << currentNode << " ";

    list<Edge>::iterator it;
    for (it = (adjacencyList[currentNode]).begin(); it != (adjacencyList[currentNode]).end(); it++){
      if (!visited[it->index]){
        visited[it->index] = true;
        q.push(it->index);
      }
    }
  }

}



//recursive
void Graph::breathFirstSearchRecurse(Type& node){
  if (visited[node]){
    return;
  }
  visited[node] = true;

  list<Edge>::iterator it;

  for (it = (adjacencyList[node]).begin(); it != (adjacencyList[node]).end(); ++it){
    cout << it->index << " ";
  }

  for (it = (adjacencyList[node]).begin(); it != (adjacencyList[node]).end(); ++it){
    breathFirstSearchRecurse(it->index);
  }

}


//gets shortest path to all nodes from starting node
void Graph::shortestPath(Type& startNode){
  if (adjacencyList.find(startNode) == adjacencyList.end()){
    return;
  }
  clearVisited();
  clearDist();


  for (const auto& pair : adjacencyList){
    distance[pair.first] = INT32_MAX;
  }
  distance[startNode] = 0;


  set<pair<int, Type>> nodeSet;
  nodeSet.insert({0, startNode});

  while (!nodeSet.empty()) {
    Type currentNode = nodeSet.begin()->second;
    nodeSet.erase(nodeSet.begin());

    visited[currentNode] = true;

    for (const auto& edge : adjacencyList[currentNode]) {
      if (!visited[edge.index] && distance[currentNode] + edge.weight < distance[edge.index]) {
        nodeSet.erase({distance[edge.index], edge.index});
        distance[edge.index] = distance[currentNode] + edge.weight;
        nodeSet.insert({distance[edge.index], edge.index});
      }
    }
  }

  for (const auto& pair : distance) {
    if (pair.second == INT_MAX) {
      cout << "Distance from " << startNode << " to " << pair.first << " is infinity" << endl;
    } else {
      cout << "Distance from " << startNode << " to " << pair.first << " is " << pair.second << endl;
    }
  }
}
