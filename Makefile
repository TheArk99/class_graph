CXX := g++
CXXFlags := -Wall -g -pipe -O2 -march=native -D_GLIBCXX_ASSERTIONS -std=c++20

.PHONY: all clean

all: main

main: src/main.cpp
	$(CXX) -o bin/main src/main.cpp src/graph.cpp $(CXXFlags)

install: clean
	mkdir -p bin/
	$(MAKE) all


clean:
	rm -rf bin/
